# Installation Instructions

There is a vipc under releases that wil install all 3 packages.

[Get Latest Release](https://gitlab.com/sas_public/class_refactoring_tools/-/releases)

# Dependencies
This tool consists of 3 packages.
There is a .vpic file in the repository root.
Source code for the other packages is here:
* [Class Refactoring Library](https://gitlab.com/sas_public/class_refactoring_lib)
* [Mock Object Helper](https://gitlab.com/sas_public/mock-object-helper)



# Demo
Here is a link to a Youtube Demo, Explaining how to generate and use an Interface Class and Mock Object.


[![Demo](http://img.youtube.com/vi/WjHZleG6nfk/0.jpg)](http://www.youtube.com/watch?v=WjHZleG6nfk "Demo")

# Limitation
The toolkit will not work for classes in PPLs.  If you want to create a mock for them, you have to make one for the source class (if you have access) and then point it to the ppl. This will/ get fixed when I upgrade it to 2020 and account for interfaces.

# CLA Summit Presentation
Here is a presentation I did on this project at the CLA Summit.


[![CLA Summit Presentation](http://img.youtube.com/vi/wA2hGvEoIWk/0.jpg)](http://www.youtube.com/watch?v=wA2hGvEoIWk "CLA Summit Presentation")


